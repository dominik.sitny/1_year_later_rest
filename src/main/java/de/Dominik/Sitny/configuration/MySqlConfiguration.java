package de.Dominik.Sitny.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class MySqlConfiguration {

	// Review Florian Bardehle: check!
	/**
	 * The Data Source Object for the configuration and connection to the MySQL
	 * Database.
	 * @return The generated Datasource Object.
	 */
	@Bean
	public DataSource mysqlDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/1_Year_Later");
		dataSource.setUsername("root");
		dataSource.setPassword("dominik01");

		return dataSource;
	}

	/**
	 * This method generated the Jdbc Template, based on the Datasource object.
	 * @return The generated Jdbc Template.
	 */
	@Bean
	public JdbcTemplate getJdbcTemplate() {
	  return new JdbcTemplate(this.mysqlDataSource());
	}

}

