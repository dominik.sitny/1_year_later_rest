package de.Dominik.Sitny.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import de.Dominik.Sitny.configuration.MongoDbConfiguration;
import de.Dominik.Sitny.database.model.User;

import static com.mongodb.client.model.Filters.eq;


/**
 * 
 * @author dominiksitny
 * In dieser Klasse werden die Documents aus der Collection Benutzer manipuliert
 */
public class UserHandler {

	//Review: Entfernen von nicht verwendeten Imports
	//Programmiersprache: Englisch
	//Entfernen von Leerflächen
	//Verbesserter Methodenname

	private MongoCollection<Document> coll;
	public UserHandler() {
		try {
			coll = getUserCollection();
		} catch (Exception e) {
			System.out.println("huhu");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// Alle Informationen eines Users
	public User getUserInfoFromUserId(String id, boolean isAdmin) {
		Document doc = getDocumentFromUserId(null, id);
		User b1 = new User();
        if (doc != null) {
        b1.setId(doc.get("_id").toString());
        b1.setAdmin(isAdmin);
        b1.setUsername(doc.getString("username"));
        b1.setPassword(doc.getString("password"));
        b1.setName(doc.getString("name"));
        b1.setState(doc.getList("status", Integer.class));
        b1.setFriendslist(doc.getList("friends", String.class));              
        } else {
            System.out.println("Fehler");
        } 
		return b1;
	}

	//Review Florian Bardehle: Leere Flächen
	// Alle Usernames in einer List, um zu prüfen, ob bei der Registrierung der User einen usernamen gewählt hat, der schon vergeben ist.
	public ArrayList<String> allUserNames(){
		ArrayList<String> usernames = new ArrayList<>();
		MongoCollection<Document> coll = getUserCollection();
		FindIterable<Document> docs = coll.find();
		for(Document doc : docs) {
			usernames.add(doc.getString("username"));
		}
		return usernames;
	}

	//Review Florian Bardehle: Code Styling
	// Benutzername und Passwort eines Benutzers
	public User getUsernameAndPassword(String username, String password) {
		User b1 = new User();
		FindIterable<Document> docs = coll.find();
		for(Document doc : docs) {
			if(doc.getString("username").equalsIgnoreCase(username) && doc.getString("password").equals(password)) {
				b1 = getUserInfoFromUserId(doc.get("_id").toString(), false);
				return b1;
			}
		}
		b1.setId("0");
		return b1;
	}

	//Review Florian Bardehle: Code styling, verbessern des if else, status wird nicht genutzt
	// Alle Freunde eines Users
	public ArrayList<User> getFriends(String id) {
		Document doc = getDocumentFromUserId(null, id);
		ArrayList<User> bList = new ArrayList<>();
		if (doc != null) {
			List<String> idsFriends = doc.getList("friends", String.class);
			if(idsFriends != null) {
				for(String ids : idsFriends) {
					bList.add(getUserInfoFromUserId(ids, false));
				}
			}
		} else {
			System.out.println("Fehler");
		}
		return bList;
	}

	//Review Dominik Sitny: Code Style verbessern, Entfernen voon Platzhalter
	// Ein neuer User wird angelegt
	public void newUser(User b1) {
		Map<String, Object> map = new HashMap<>();
		map.put("username", b1.getUsername());
		map.put("password", b1.getPassword());	
		map.put("name", b1.getName());
		map.put("friends", null);
		map.put("status", null);
		Document doc = new Document(map);
		coll.insertOne(doc); 	
	}



	//Review Dominik Sitny: Code Styling, docs ist immer null, Entfernen von prints
	// Alle Benutzer werden ausgegeben die den Wert von String name beinhalten. 
	public ArrayList<User> getSearchedUsers(String name){
		ArrayList<User> bList = new ArrayList<>();
		FindIterable<Document> docs = coll.find(Filters.regex("username", Pattern.compile(name+"*", Pattern.CASE_INSENSITIVE)));

		for(Document doc : docs) {
			User b1 = new User();
			b1.setId(doc.get("_id").toString());
			b1.setAdmin(false);
			b1.setUsername(doc.getString("username"));
			b1.setPassword(doc.getString("password"));
			b1.setName(doc.getString("name"));
			b1.setState(doc.getList("status", Integer.class));
			b1.setFriendslist(doc.getList("friends", String.class));
			bList.add(b1);
		}

		return bList;

	}

	// Review Florian Bardehle: Entfernen von prints, Coe Style, Nullpointer möglich!, better switch case
	// else if userstate = -1 ist immer true
	// Hier wird der Status des Freundes geändert. Möglich sind vier Stati: wartend, jetzt akzeptieren, befreundet und nicht befreundet
	public void changeFriendState(String userId, String friendId, int newValueUserState) {
		Bson filter = eq("_id", new ObjectId(userId));
		Document doc = getDocumentFromUserId(null, userId);
		assert doc != null;
		List<String> list = doc.getList("friends", String.class);
		List<Integer> listStatus = doc.getList("status", Integer.class);

		switch (newValueUserState) {
			case 0, 1 -> {
				if (list == null) {
					list = new ArrayList<>();
					listStatus = new ArrayList<>();
				}
				list.add(friendId);
				listStatus.add(newValueUserState);
				doc.replace("friends", list);
				doc.replace("status", listStatus);
			}
			case 2, -1 -> {
				for (int i = 0; i < list.size(); i++) {
					if (friendId.equals(list.get(i))) {
						if (newValueUserState == 2) {
							listStatus.set(i, 2);
						} else {
							list.remove(i);
							listStatus.remove(i);

							if (listStatus.size() == 0) {
								listStatus = null;
								doc.replace("friends", null);
								break;
							}

						}
					}
				}
				doc.replace("status", listStatus);
			}
		}
		coll.findOneAndReplace(filter, doc);
	}
	
	// Änderungen von Name und Passwort des Users 
	public void updateUser(String id, String passwort) {
		Bson filter = eq("_id", new ObjectId(id));	
		Document doc = getDocumentFromUserId(null, id);
		doc.replace("password", passwort);
		
		coll.findOneAndReplace(filter, doc);	
	}
	
	
	// Das Dokument des Users in der DB wird gelöscht
	public void deleteUser(String userId) {
		coll.deleteOne((Filters.eq("_id", new ObjectId(userId))));
	}


	// Alle Documents aus der Collection Gruppe werden zurückgegeben
	public static MongoCollection<Document> getUserCollection(){
		MongoDatabase database = MongoDbConfiguration.getMongoDatabase();
		return database.getCollection("Benutzer");
	}

	// Um keine Dopplungen im Code zu haben, wurde diese Zeile Code in eine Funktion geändert. 
	// Rückgabewert sind die Informationen des Users
	public Document getDocumentFromUserId(User user, String userId) {

		if (userId==null) {
			return coll.find(Filters.eq("_id",new ObjectId(user.getId()))).first();
		}
		else {
			return coll.find(Filters.eq("_id",new ObjectId(userId))).first();
		}
	}


}
