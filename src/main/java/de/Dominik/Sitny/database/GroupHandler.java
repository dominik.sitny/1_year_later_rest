package de.Dominik.Sitny.database;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import de.Dominik.Sitny.configuration.MongoDbConfiguration;
import de.Dominik.Sitny.database.model.Group;
import de.Dominik.Sitny.database.model.User;


/**
 * 
 * @author dominiksitny
 * In dieser Klasse werden die Documents aus der Collection Group manipuliert
 */
public class GroupHandler {

	private MongoCollection<Document> coll;

	private final UserHandler userHandler = new UserHandler();
	public GroupHandler() {
		try {
			coll = getGroupCollection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	 // Alle Gruppen eines Users  
	public ArrayList<Group> getGroupsFromId(String id, boolean group) {
		ArrayList<Group>list = new ArrayList<>();
		BasicDBObject searchQuery = new BasicDBObject();
		if(group) {
			searchQuery.put("_id", new ObjectId(id));
		}else {			
			searchQuery.put("teilnehmer", id);	
		}
		try (MongoCursor<Document> cursor = coll.find(searchQuery).iterator()) {
			while (cursor.hasNext()) {
				Document document = cursor.next();
				Group g = new Group();
				g.setId(document.get("_id").toString());
				g.setName(document.getString("name"));
				g.setCreationDate(document.getDate("erstellungsdatum"));
				List<String> idUser = document.getList("teilnehmer", String.class);
				ArrayList<User> bList = new ArrayList<User>();
				List<String> adminList = document.getList("admin", String.class);
				for (String s : idUser) {
					bList.add(userHandler.getUserInfoFromUserId(s.toString(), adminList.contains(s.toString())));
				}
				g.setMemberList(bList);
				list.add(g);
			}
		}
		return list;
	}

	// Eine neue Gruppe wird erstellt
	public void saveNewGroup(Group group) {
		ArrayList<String>listTeilnehmer = new ArrayList<>();
		ArrayList<String>listAdmin = new ArrayList<>();
		Map<String,Object>map = new HashMap<String, Object>();	
		map.put("name", group.getName());
		map.put("erstellungsdatum", group.getCreationDate());
		
		for (User u : group.getMemberList()){
			listTeilnehmer.add(u.getId());
			if(u.isAdmin()) {
				listAdmin.add(u.getId());
			}
		}
		
		map.put("teilnehmer",listTeilnehmer);
		map.put("admin", listAdmin);
		Document doc = new Document(map);
		coll.insertOne(doc); 		
	}
	
	// Prüfung, ob User admin einer Gruppe ist
	public boolean isAdmin(String userId, String groupId) {
		Document doc = getDocumentFromGroupId(null, groupId);
		
		for(String id : doc.getList("admin", String.class)) {
			if (id.equalsIgnoreCase(userId)) {
				return true;
			}
		}
		return false;
	
	}
	
	// Löschen einer Gruppe
	public void deleteGroup(String groupId) {
		coll.deleteOne((Filters.eq("_id", new ObjectId(groupId))));
	}
	
	//User verlässt die Gruppe
	public void leaveGroup(String userId, String groupId) {
		Bson andComparison = Filters.and(Filters.eq("_id", new ObjectId(groupId)), Filters.eq("teilnehmer", userId));
		Document doc = getDocumentFromGroupId(null, groupId);
		for (int i = 0; i<doc.getList("teilnehmer", String.class).size(); i++) {
			if(doc.getList("teilnehmer", String.class).get(i).equalsIgnoreCase(userId)) {
				doc.getList("teilnehmer", String.class).remove(i);
			}
		}
		coll.findOneAndReplace(andComparison, doc);
	}
	
	//Freunde des Gruppenadmins werden in die Gruppe hinzugefügt
	public void addFriendsToGroup(String groupId, ArrayList<String> idList) {
		Bson filter = Filters.eq("_id", new ObjectId(groupId));		
		Document doc = getDocumentFromGroupId(null, groupId);
		for(String id : idList) {			
			doc.getList("teilnehmer",String.class).add(id);			
		}
		coll.findOneAndReplace(filter, doc);
	}
	// Review Florian: Das Gleiche wie leaveGroup()?
	// Dominik:  Nein
	// Ein Teilnehmer wird aus der Gruppe entfernt
	public void removeMember(String groupId, String memberId) {
		Bson filter = Filters.eq("_id", new ObjectId(groupId));
		Document doc = getDocumentFromGroupId(null, groupId);
		
		for(int i=0;i<doc.getList("teilnehmer",String.class).size(); i++) {
			if(memberId.equals(doc.getList("teilnehmer",String.class).get(i))) {
				doc.getList("teilnehmer",String.class).remove(i);
			}
		}
		coll.findOneAndReplace(filter, doc);	
	}
	
	public void updateGroup(String id, String name) {
		
		Bson filter = eq("_id", new ObjectId(id));	
		Document doc = getDocumentFromGroupId(null, id);		
		doc.replace("name", name);
		coll.findOneAndReplace(filter, doc);		
	}
	
	// Admin einer Gruppe wird hinzugefügt
	public void addAdmin(String groupId, ArrayList<String> userIds) {
		Bson filter = Filters.eq("_id", new ObjectId(groupId));
		Document doc = getDocumentFromGroupId(null, groupId);
		
		for (String id : userIds) {
			doc.getList("admin", String.class).add(id);
		}
		
		
		coll.findOneAndReplace(filter, doc);
	}
	
	// Alle Documents aus der Collection Gruppe werden zurückgegeben
	public MongoCollection<Document>getGroupCollection(){
		MongoDatabase database = MongoDbConfiguration.getMongoDatabase();
		return database.getCollection("Gruppen");	
	}
	
	// Um keine Dopplungen im Code zu haben, wurde diese Zeile Code in eine Funktion geändert. 
	// Rückgabewert ist eine Gruppe des Users
	public Document getDocumentFromGroupId(Group group, String groupId) {

		if (groupId==null) {
			return coll.find(Filters.eq("_id",new ObjectId(group.getId()))).first();
		}
		else {
			return coll.find(Filters.eq("_id",new ObjectId(groupId))).first();
		}
	}

}
