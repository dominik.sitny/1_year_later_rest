package de.Dominik.Sitny.database.model;

import java.util.List;



public class User {
	//Review Florian Bardehle: check
	private String id;
	private String username;
	private String password;
	private String name;
	private List<String> friendslist;
	private List<Integer> state;
	
	private boolean isAdmin;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getFriendslist() {
		return friendslist;
	}
	public void setFriendslist(List<String> friendslist) {
		this.friendslist = friendslist;
	}
	public List<Integer> getState() {
		return state;
	}
	public void setState(List<Integer> state) {
		this.state = state;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
		
	
}
