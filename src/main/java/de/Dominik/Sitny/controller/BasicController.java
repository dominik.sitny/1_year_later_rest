package de.Dominik.Sitny.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.Dominik.Sitny.database.UserHandler;
import de.Dominik.Sitny.database.GroupHandler;
import de.Dominik.Sitny.database.model.Group;
import de.Dominik.Sitny.database.model.User;


	@RestController
	public class BasicController {

		private UserHandler userHandler = new UserHandler();
		private GroupHandler groupHandler = new GroupHandler();
		
		//testen
		//Review Dominik Sitny: Vereinfachte Schreibweise!
		@GetMapping("/getUser")
		public User getUser(@RequestParam String id) {
			return userHandler.getUserInfoFromUserId(id, false);
		}

		//Review Florian Bardehle: geFriends -> getFriends
		@GetMapping("/getFriends")
		public ArrayList<User> getFriends(@RequestParam String id){
			return userHandler.getFriends(id);
		}
		
		//testen
		//Review Florian Bardehle: entfernen Leerzeile
		@GetMapping("/getAllUserNames")
		public ArrayList<String> getAllUserNames() {
			return userHandler.allUserNames();
		}

		//Review Florian Bardehle: Programmiersprache Englisch
		@GetMapping("/getGroups")
		public ArrayList<Group> getGroupsOfUser(@RequestParam String id, boolean group) {
			return groupHandler.getGroupsFromId(id, group);
		}

		//Review Florian Bardehle: typo
		@PostMapping("/newGroup")
		public void newGroup(@RequestBody Group g) {
			groupHandler.saveNewGroup(g);
		}
		
		//testen
		//Review Dominik Sitny: Programmiersprache Englisch
		@PostMapping("/newUser")
		public void newUser(@RequestParam String username, String password, String name) {
			User b1 = new User();
			b1.setUsername(username);
			b1.setPassword(password);
			b1.setName(name);
			userHandler.newUser(b1);
		}
		
		//testen
		//Review Oliver Wiesner: Kein printen von Logindaten in der Konsole
		@GetMapping("/LogIn")
		public User checkLogIn(@RequestParam String username, String password) {				
			return userHandler.getUsernameAndPassword(username, password);
			
		}

		
		//Review Oliver Wiesner: entfernen von leeren Flächen
		@GetMapping("/getSearchedUsernames")
		public ArrayList<User> searchUsers(@RequestParam String username){
			if(username.length()<2) {
				ArrayList<User> b = new ArrayList<>();
				User b2 = new User();
				b2.setUsername("");
				b2.setId("0");
				b2.setName("");
				b2.setAdmin(false);
				b.add(b2);
				return b;
			}			
			return userHandler.getSearchedUsers(username);
		}

		//Review Florian Bardehle: Entfernen von print, verbessern code Style
		@PutMapping("/changeFS/{id}")
		public String sendFa(@PathVariable("id") String id, @RequestBody User friendUser) {
			switch (friendUser.getState().get(0)) {
				case -1, 1 -> {
					//System.out.println("case -1 | 1");
					userHandler.changeFriendState(id, friendUser.getId(), -1); //User
					userHandler.changeFriendState(friendUser.getId(), id, -1);    //Friend
					return "case -1 | 1";
				}
				case 0 -> {
					//System.out.println("case 0");
					userHandler.changeFriendState(id, friendUser.getId(), 0); //User
					userHandler.changeFriendState(friendUser.getId(), id, 1);    //Friend
					return "case 0";
				}
				case 2 -> {
					//System.out.println("case 2");
					userHandler.changeFriendState(id, friendUser.getId(), 2); //User
					userHandler.changeFriendState(friendUser.getId(), id, 2);    //Friend
					return "case 2";
				}
			}
			return "fehlgeschlagen :(";
		}
		
		@GetMapping("/isAdmin")
		public boolean isAdmin(@RequestParam String userId, String groupId) {
			return groupHandler.isAdmin(userId, groupId);
		}
		
		@DeleteMapping("/deleteGroup/{id}")
		public void deleteGroup(@PathVariable String id) {
			groupHandler.deleteGroup(id);
		}
		
		@PostMapping("/leaveGroup")
		public void leaveGroup(@RequestParam String userId, String groupId) {
			groupHandler.leaveGroup(userId, groupId);
		}
		
		@PostMapping("/addFriendsToGroup/{groupId}")
			public void addFriendsToGroup(@PathVariable String groupId, @RequestBody ArrayList<String>idsFriends) {
				groupHandler.addFriendsToGroup(groupId, idsFriends);
			}
		
	
		@PostMapping("/removeUserFromGroup/{groupId}")
			public void removeUserFromGroup(@PathVariable String groupId,@RequestBody User user) {
				groupHandler.removeMember(groupId, user.getId());
		}
	
		@PostMapping("/addAdmin/{groupId}")
			public void addAdmin(@PathVariable String groupId, @RequestBody ArrayList<String> userIds) {
				groupHandler.addAdmin(groupId, userIds);
		}
		
		@PostMapping("/updateGroup")
		public void updateGroup(@RequestParam String id, String name) {
			System.out.println("update läuft");
			groupHandler.updateGroup(id, name);
	}
		
		@PostMapping("/updateUser")
			public void updateUser(@RequestParam String id, String password) {
				userHandler.updateUser(id, password);
		}
		
		@DeleteMapping("/deleteUser/{id}")
			public void deleteUser(@PathVariable String id) {
		}
		
}