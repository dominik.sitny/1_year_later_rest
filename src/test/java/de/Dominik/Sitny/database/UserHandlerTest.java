package de.Dominik.Sitny.database;
import de.Dominik.Sitny.database.model.User;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.*;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

public class UserHandlerTest {
    private String testUserId;
    private User testUser;

    private UserHandler userHandler;

    private String friendID;

    @BeforeAll
    static void setUp() {
        // set up any necessary resources before all tests
    }

    @BeforeEach
    void setUpBeforeEach() {
        friendID = "63bc9cc861531e716e5b99ef";
        userHandler = new UserHandler();
        // set up any necessary resources before each test
        testUserId = "63ce2e3a79caeb46d4d26585";
        testUser = new User();
        testUser.setId(testUserId);
        testUser.setUsername("DominikSitny");
        testUser.setPassword("test1234");
        testUser.setName("Dominik Sitny");
        testUser.setFriendslist(new ArrayList<String>());
    }

    @Test
    void testGetUserinfosFromUserId() {
        User user = userHandler.getUserInfoFromUserId(testUserId, false);
        assertEquals(testUser.getId(), user.getId());
    }

    @Test
    void testAllUserNames() {
        ArrayList<String> usernames = userHandler.allUserNames();
        assertNotNull(usernames);
        assertFalse(usernames.isEmpty());
    }

    @Test
    void testGetUsernameAndPassword() {
        User user = userHandler.getUsernameAndPassword("DominikSitny", "test1234");
        assertEquals(testUser.getUsername(), user.getUsername());
    }

    @Test
    void testGetFriends() {
        ArrayList<User> friends = userHandler.getFriends(testUserId);
        assertNotNull(friends);
        assertTrue(!friends.isEmpty());
    }

    @Test
    void testNewUser() {
        userHandler.newUser(testUser);
        User user = userHandler.getUserInfoFromUserId(testUserId, false);
        assertEquals(testUser.getId(), user.getId());
    }

    @Test
    void testGetSearchedUsers() {
        ArrayList<User> searchedUsers = userHandler.getSearchedUsers("DominikSitny");
        assertNotNull(searchedUsers);
        assertFalse(searchedUsers.isEmpty());
    }


    @Test
    public void testChangeFriendState() {
        friendID = "63ce2e1279caeb46d4d26584";
        userHandler.changeFriendState(testUser.getId(), friendID, 1);

        // Verify that the friend was added to the user's friend list with status 1
        User updatedUser = userHandler.getUserInfoFromUserId(testUser.getId(), true);
        assertEquals(2, updatedUser.getState().get(0).intValue());
        assertEquals(friendID, updatedUser.getFriendslist().get(1));

        // Change the friend's status to 2
        userHandler.changeFriendState(testUser.getId(), friendID, 2);

        // Verify that the friend's status was updated to 2
        updatedUser = userHandler.getUserInfoFromUserId(testUser.getId(), true);
        assertEquals(2, updatedUser.getState().get(0).intValue());

        // Remove the friend from the user's friend list
        userHandler.changeFriendState(testUser.getId(), friendID, -1);
    }

    @AfterEach
    void tearDownAfterEach() {
        // tear down any resources after each test
    }

    @AfterAll
    static void tearDown() {
        // tear down any resources after all tests
    }
}
