package de.Dominik.Sitny.database;

import de.Dominik.Sitny.database.model.Group;
import de.Dominik.Sitny.database.model.User;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GroupHandlerTest {

    private Group testGroup;

    private GroupHandler groupHandler;

    private ArrayList<User> memberList;

    private ArrayList<String> memberIdList;

    @BeforeEach
    public void setUp() {
        groupHandler = new GroupHandler();
        testGroup = new Group();
        testGroup.setName("TestGruppe");
        testGroup.setId("63bc969965739d3a3f7b7eec");
        memberList = new ArrayList<>();
        User user1 = new User();
        user1.setId("63bc969965739d3a3f7b7eec");
        user1.setName("User 1");
        User user2 = new User();
        user2.setId("2");
        user2.setName("User 2");
        memberList.add(user1);
        memberIdList = new ArrayList<>();
        memberIdList.add("63ce2e3a79caeb46d4d26585");
        //memberList.add(user2);
        testGroup.setMemberList(memberList);
    }

    @AfterEach
    public void tearDown() {
        groupHandler = null;
    }

    @Test
    public void testSaveNewGroup() {
        groupHandler.saveNewGroup(testGroup);
        ArrayList<Group> groups = groupHandler.getGroupsFromId("63bc969965739d3a3f7b7eec", false);
        assertEquals(testGroup.getName(), groups.get(groups.size()-1).getName());
    }

    @Test
    public void testGetGroupsFromId() {
        ArrayList<Group> groups = groupHandler.getGroupsFromId("63bc9cc861531e716e5b99ef", false);
        assertEquals(5, groups.size());
    }

    @Test
    public void testIsAdmin() {
        assertEquals(true, groupHandler.isAdmin("63bc9cc861531e716e5b99ef","63bc969965739d3a3f7b7eec"));
    }

    @Test
    public void testAddFriendsToGroup() {
        groupHandler.addFriendsToGroup("63fb6cebddade742910643a1", memberIdList);
        ArrayList<Group> groups = groupHandler.getGroupsFromId("63fb6cebddade742910643a1", true);
        assertEquals(2, groups.get(0).getMemberList().size());
        groupHandler.removeMember("63fb6cebddade742910643a1", "63ce2e3a79caeb46d4d26585");
    }

    @Test
    public void testRemoveFriends() {
        groupHandler.addFriendsToGroup("63fb6cebddade742910643a1", memberIdList);
        groupHandler.removeMember("63fb6cebddade742910643a1", "63ce2e3a79caeb46d4d26585");
        ArrayList<Group> groups = groupHandler.getGroupsFromId("63fb6cebddade742910643a1", true);
        assertEquals(1, groups.get(0).getMemberList().size());
    }

}
