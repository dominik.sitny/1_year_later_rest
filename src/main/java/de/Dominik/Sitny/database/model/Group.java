package de.Dominik.Sitny.database.model;



import java.util.Date;
import java.util.List;

public class Group {
	//Review Florian Bardehle: Check!
	String id;
	String name;
	List<User> memberList; 
	Date creationDate;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<User> memberList) {
		this.memberList = memberList;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	

	

	
	
	
	
	
	
	
}
