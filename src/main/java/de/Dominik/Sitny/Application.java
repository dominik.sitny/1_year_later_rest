package de.Dominik.Sitny;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	//Review Florian Bardehle: check!
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
